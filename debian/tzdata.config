#! /bin/sh
set -e

. /usr/share/debconf/confmodule
db_version 2.0
db_capb backup

convert_timezone()
{
	case "$1" in
		right/* | posix/*)
			convert_timezone "${1#*/}"
			;;
		"Africa/Asmera")
			echo "Africa/Asmara"
			;;
		"America/Buenos_Aires")
			echo "America/Argentina/Buenos_Aires"
			;;
		"America/Argentina/ComodRivadavia" | "America/Catamarca")
			echo "America/Argentina/Catamarca"
			;;
		"America/Cordoba" | "America/Rosario")
			echo "America/Argentina/Cordoba"
			;;
		"America/Fort_Wayne" | "America/Indianapolis" | "SystemV/EST5" | "US/East-Indiana")
			echo "America/Indiana/Indianapolis"
			;;
		"America/Godthab")
			echo "America/Nuuk"
			;;
		"America/Jujuy")
			echo "America/Argentina/Jujuy"
			;;
		"America/Knox_IN" | "US/Indiana-Starke")
			echo "America/Indiana/Knox"
			;;
		"America/Louisville")
			echo "America/Kentucky/Louisville"
			;;
		"America/Mendoza")
			echo "America/Argentina/Mendoza"
			;;
		"Antarctica/South_Pole")
			echo "Antarctica/McMurdo"
			;;
		"Asia/Ashkhabad")
			echo "Asia/Ashgabat"
			;;
		"Asia/Calcutta")
			echo "Asia/Kolkata"
			;;
		"Asia/Choibalsan")
			echo "Asia/Ulaanbaatar"
			;;
		"Asia/Chungking")
			echo "Asia/Chongqing"
			;;
		"Asia/Dacca")
			echo "Asia/Dhaka"
			;;
		"Asia/Katmandu")
			echo "Asia/Kathmandu"
			;;
		"Asia/Macao")
			echo "Asia/Macau"
			;;
		"Asia/Rangoon")
			echo "Asia/Yangon"
			;;
		"Asia/Riyadh87" | "Asia/Riyadh88" | "Asia/Riyadh89" | "Mideast/Riyadh87" | "Mideast/Riyadh88" | "Mideast/Riyadh89")
			echo "Asia/Riyadh"
			;;
		"Asia/Saigon")
			echo "Asia/Ho_Chi_Minh"
			;;
		"Asia/Thimbu")
			echo "Asia/Thimphu"
			;;
		"Asia/Ujung_Pandang")
			echo "Asia/Makassar"
			;;
		"Asia/Ulan_Bator")
			echo "Asia/Ulaanbaatar"
			;;
		"Atlantic/Faeroe")
			echo "Atlantic/Faroe"
			;;
		"Australia/ACT" | "Australia/NSW")
			echo "Australia/Sydney"
			;;
		"Australia/LHI")
			echo "Australia/Lord_Howe"
			;;
		"Australia/North")
			echo "Australia/Darwin"
			;;
		"Australia/Queensland")
			echo "Australia/Brisbane"
			;;
		"Australia/South")
			echo "Australia/Adelaide"
			;;
		"Australia/Tasmania")
			echo "Australia/Hobart"
			;;
		"Australia/Victoria")
			echo "Australia/Melbourne"
			;;
		"Australia/West")
			echo "Australia/Perth"
			;;
		"Brazil/Acre")
			echo "America/Rio_Branco"
			;;
		"Brazil/DeNoronha")
			echo "America/Noronha"
			;;
		"Brazil/East")
			echo "America/Sao_Paulo"
			;;
		"Brazil/West")
			echo "America/Manaus"
			;;
		"Canada/Atlantic" | "SystemV/AST4ADT")
			echo "America/Halifax"
			;;
		"Canada/Central")
			echo "America/Winnipeg"
			;;
		"Canada/Eastern")
			echo "America/Toronto"
			;;
		"Canada/East-Saskatchewan" | "Canada/Saskatchewan" | "SystemV/CST6")
			echo "America/Regina"
			;;
		"Canada/Mountain")
			echo "America/Edmonton"
			;;
		"Canada/Newfoundland")
			echo "America/St_Johns"
			;;
		"Canada/Pacific")
			echo "America/Vancouver"
			;;
		"Canada/Yukon")
			echo "America/Whitehorse"
			;;
		"CET" | "MET")
			echo "Europe/Brussels"
			;;
		"Chile/Continental")
			echo "America/Santiago"
			;;
		"Chile/EasterIsland")
			echo "Pacific/Easter"
			;;
		"CST6CDT" | "SystemV/CST6CDT" | "US/Central")
			echo "America/Chicago"
			;;
		"Cuba")
			echo "America/Havana"
			;;
		"Egypt")
			echo "Africa/Cairo"
			;;
		"EET")
			echo "Europe/Athens"
			;;
		"Eire")
			echo "Europe/Dublin"
			;;
		"EST")
			echo "America/Panama"
			;;
		"EST5EDT" | "SystemV/EST5EDT" | "US/Eastern")
			echo "America/New_York"
			;;
		"Europe/Kiev")
			echo "Europe/Kyiv"
			;;
		"Europe/Uzhgorod")
			echo "Europe/Kyiv"
			;;
		"Europe/Zaporozhye")
			echo "Europe/Kyiv"
			;;
		"GB" | "GB-Eire")
			echo "Europe/London"
			;;
		"GMT")
			echo "Etc/GMT"
			;;
		"GMT0")
			echo "Etc/GMT0"
			;;
		"GMT-0")
			echo "Etc/GMT-0"
			;;
		"GMT+0")
			echo "Etc/GMT+0"
			;;
		"Greenwich")
			echo "Etc/Greenwich"
			;;
		"Hongkong")
			echo "Asia/Hong_Kong"
			;;
		"HST" | "SystemV/HST10" | "US/Hawaii")
			echo "Pacific/Honolulu"
			;;
		"Iceland")
			echo "Atlantic/Reykjavik"
			;;
		"Iran")
			echo "Asia/Tehran"
			;;
		"Israel")
			echo "Asia/Tel_Aviv"
			;;
		"Jamaica")
			echo "America/Jamaica"
			;;
		"Japan")
			echo "Asia/Tokyo"
			;;
		"Kwajalein")
			echo "Pacific/Kwajalein"
			;;
		"Libya")
			echo "Africa/Tripoli"
			;;
		"Mexico/BajaNorte")
			echo "America/Tijuana"
			;;
		"Mexico/BajaSur")
			echo "America/Mazatlan"
			;;
		"Mexico/General")
			echo "America/Mexico_City"
			;;
		"MST" | "SystemV/MST7" | "US/Arizona")
			echo "America/Phoenix"
			;;
		"MST7MDT" | "Navajo" | "SystemV/MST7MDT" | "US/Mountain")
			echo "America/Denver"
			;;
		"NZ")
			echo "Pacific/Auckland"
			;;
		"Pacific/Enderbury")
			echo "Pacific/Kanton"
			;;
		"Pacific/Ponape")
			echo "Pacific/Pohnpei"
			;;
		"Pacific/Truk")
			echo "Pacific/Chuuk"
			;;
		"NZ-CHAT")
			echo "Pacific/Chatham"
			;;
		"Poland")
			echo "Europe/Warsaw"
			;;
		"Portugal" | "WET")
			echo "Europe/Lisbon"
			;;
		"PRC")
			echo "Asia/Shanghai"
			;;
		"PST8PDT" | "SystemV/PST8PDT" | "US/Pacific")
			echo "America/Los_Angeles"
			;;
		"ROC")
			echo "Asia/Taipei"
			;;
		"ROK")
			echo "Asia/Seoul"
			;;
		"Singapore")
			echo "Asia/Singapore"
			;;
		"SystemV/AST4")
			echo "America/Puerto_Rico"
			;;
		"SystemV/PST8")
			echo "Pacific/Pitcairn"
			;;
		"SystemV/YST9")
			echo "Pacific/Gambier"
			;;
		"SystemV/YST9YDT" | "US/Alaska")
			echo "America/Anchorage"
			;;
		"Turkey")
			echo "Europe/Istanbul"
			;;
		"UCT")
			echo "Etc/UCT"
			;;
		"Universal" | "UTC" | "Zulu")
			echo "Etc/UTC"
			;;
		"US/Aleutian")
			echo "America/Adak"
			;;
		"US/Michigan")
			echo "America/Detroit"
			;;
		"US/Samoa")
			echo "Pacific/Pago_Pago"
			;;
		"W-SU")
			echo "Europe/Moscow"
			;;
		*)
			echo "$1"
			;;
	esac
}

validate_timezone() {
	local AREA="${1%%/*}"
	local ZONE="${1#*/}"

	test -n "$AREA" || return 1
	test -n "$ZONE" || return 1

	db_metaget tzdata/Areas choices-c || return 1
	echo $RET | grep -q "\b$AREA\b" || return 1

	db_metaget tzdata/Zones/$AREA choices-c || return 1
	echo $RET | grep -q "\b$ZONE\b" || return 1

	return 0
}

# Read timezone from /etc/localtime if it is a link
if [ -L "$DPKG_ROOT/etc/localtime" ] ; then
	TIMEZONE="$(readlink "$DPKG_ROOT/etc/localtime")"
	TIMEZONE="$(cd "$DPKG_ROOT/etc" && realpath -m -s "$TIMEZONE")"
	TIMEZONE="${TIMEZONE#/usr/share/zoneinfo/}"
# Fall back to /etc/timezone if present
elif [ -e "$DPKG_ROOT/etc/timezone" ]; then
	TIMEZONE="$(head -n 1 "$DPKG_ROOT/etc/timezone")"
	TIMEZONE="${TIMEZONE%% *}"
	TIMEZONE="${TIMEZONE##/}"
	TIMEZONE="${TIMEZONE%%/}"
fi

TIMEZONE="$(convert_timezone "$TIMEZONE")"

if validate_timezone "$TIMEZONE" ; then
	AREA="${TIMEZONE%%/*}"
	ZONE="${TIMEZONE#*/}"

	# Don't ask the user, except if they explicitely asked that
	if [ -z "$DEBCONF_RECONFIGURE" ] ; then
		db_fset tzdata/Areas seen true
		db_fset "tzdata/Zones/$AREA" seen true
	fi
# The timezone has never been configured or is falsely configured
else
	# Try to use the value from debconf, the config file could have been
	# lost or the values could have been preseeded
	db_get tzdata/Areas || RET=
	if [ -n "$RET" ] ; then
		AREA=$RET
		db_get "tzdata/Zones/$AREA" || RET=
		if [ -n "$RET" ] ; then
			ZONE=$RET
		fi
	fi

	# If the values fetch from debconf are invalid, default to Etc/UTC and
	# make sure the question is displayed
	if ! validate_timezone "$AREA/$ZONE" ; then
		AREA="Etc"
		ZONE="UTC"
		db_fset tzdata/Areas seen false
		db_metaget tzdata/Areas choices-c || RET=
		for AREA in $(echo $RET | tr -d ',') ; do
			db_fset "tzdata/Zones/$AREA" seen false
		done
	fi
fi

# Initializes debconf default values from the ones found in
# configuration files
db_set tzdata/Areas "$AREA"
db_set "tzdata/Zones/$AREA" "$ZONE"

STATE=1
while [ "$STATE" -ge 0 ]; do
	case "$STATE" in
	0)
		# The user has cancel the timezone change, reset the debconf
		# values to the initial one.
		db_set tzdata/Areas "$AREA"
		db_set "tzdata/Zones/$AREA" "$ZONE"
		break
		;;
	1)
		# Ask the user of the Area
		db_input high tzdata/Areas || true
		;;
	2)
		# Ask the user of the Zone
		db_get tzdata/Areas || RET=Etc
		db_input high "tzdata/Zones/$RET" || true
		;;
	*)
		break
		;;
	esac
	# shellcheck disable=SC2119
	if db_go; then
		STATE=$((STATE + 1))
	else
		STATE=$((STATE - 1))
	fi
done

#DEBHELPER#


exit 0
